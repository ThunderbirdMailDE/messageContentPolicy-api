// Add eventListener for onClicked on the example toolbar button
messenger.messageDisplayAction.onClicked.addListener(onToolbarButtonClicked);
async function onToolbarButtonClicked(tab, info) {
  let updateProperties = {};

  updateProperties.msgBodyAs = "original";       // optional, should remain undefined, to not change the pref
  updateProperties.disableRemoteContent = true;  // optional, should remain undefined, to not change the pref
  updateProperties.attachmentsInline = false;    // optional, should remain undefined, to not change the pref

  // We have to pass in the correct tab.windowId, to make the window.ReloadMessage() function really reload the displayed message, 
  // if we want it to be reloaded (3rd parameter != true).
  // If we only pass in any window, the window.ReloadMessage() function doesn't throw an error (it seems to be existing for every window),
  // but the message is not reloaded reliable (at least in standalone windows).
  let test_button = await messenger.messageContentPolicy.update(tab.windowId, updateProperties, false);
  console.debug("test_button resolved as: ", test_button);
}

async function main() {
  let updateProperties = {};
  let window = await messenger.windows.getCurrent();
  console.debug(window);

  /* *************   1st test   ******************/
  // get the current application settings
  let test_1 = await messenger.messageContentPolicy.getCurrent();
  console.debug("test_1 resolved as: ", test_1);


  /* *************   2nd test   ******************/
  updateProperties.msgBodyAs = "original";       // optional, should remain undefined, to not change the pref
  updateProperties.disableRemoteContent = true;  // optional, should remain undefined, to not change the pref
  updateProperties.attachmentsInline = false;    // optional, should remain undefined, to not change the pref

  // update some of the application settings and (in theory) reload message, because of undefined 3rd parameter
  let test_2 = await messenger.messageContentPolicy.update(window.id, updateProperties);
  console.debug("test_2 resolved as: ", test_2);


  /* *************   3rd test   ******************/
  updateProperties.msgBodyAs = "plaintext";      // optional, should remain undefined, to not change the pref
  updateProperties.disableRemoteContent = true;  // optional, should remain undefined, to not change the pref
  updateProperties.attachmentsInline = false;    // optional, should remain undefined, to not change the pref

  // update some of the properties _without_ message reload, because of 3rd parameter = true
  let test_3 = await messenger.messageContentPolicy.update(window.id, updateProperties, true);
  console.debug("test_3 resolved as: ", test_3);


  /* *************   Possible event listeners provided by this API   ******************/
  // Do not define the parameter changedProperty.
  // The event will fire for all possible properties
  messenger.messageContentPolicy.onChanged.addListener((newValue) => {
    console.debug("Listener for all properties:", newValue);
  });

  // Define the possible values for changedProperty.
  // The event will fire only, if the related property is changed
  messenger.messageContentPolicy.onChanged.addListener((newValue) => {
    console.debug("Listener for msgBodyAs:", newValue);
  }, "msgBodyAs");
  messenger.messageContentPolicy.onChanged.addListener((newValue) => {
    console.debug("Listener for showAllBodyPartsMenuitem:", newValue);
  }, "showAllBodyPartsMenuitem");
  messenger.messageContentPolicy.onChanged.addListener((newValue) => {
    console.debug("Listener for disableRemoteContent:", newValue);
  }, "disableRemoteContent");
  messenger.messageContentPolicy.onChanged.addListener((newValue) => {
    console.debug("Listener for attachmentsInline:", newValue);
  }, "attachmentsInline");

  // Provoke an error with a not existing value for changedProperty
  messenger.messageContentPolicy.onChanged.addListener((newValue) => {
    console.debug("Listener for attachmentsInline:", newValue);
  }, "falseValue - not working");

}

main();